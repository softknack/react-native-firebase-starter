import firebase from "react-native-firebase";

class FirebaseService {
  constructor() {
    this.ref = firebase.firestore().collection("people");
    console.log(this.ref);
  }

  load() {
    console.log(this.ref);
    const defaultDoc = {
      name: "ABC",
      age: 3
    };
    this.ref.doc("1").set(defaultDoc);
    console.log(
      this.ref
        .doc("1")
        .get()
        .then(snapshot => {
          console.log(snapshot);
        })
    );

    // Create a query against the collection.
    var query = this.ref.where("name", "==", "ABC");

    this.ref
      .where("name", "==", "ABC")
      .get()
      .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, " => ", doc.data());
        });
      })
      .catch(function(error) {
        console.log("Error getting documents: ", error);
      });
  }
}
export const firebaseService = new FirebaseService();
