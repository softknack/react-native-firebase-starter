import React from "react";
import { StyleSheet, Platform, Image, Text, View } from "react-native";
import firebase from "react-native-firebase";

export default class Main extends React.Component {
  state = { currentUser: null };

  componentDidMount() {
    firebase
      .auth()
      .signOut()
      .then(
        function() {
          // Sign-out successful.
        },
        function(error) {
          // An error happened.
        }
      );

    const { currentUser } = firebase.auth();

    this.setState({ currentUser });
  }

  render() {
    const { currentUser } = this.state;

    return (
      <View style={styles.container}>
        <Text>Hi 123 {currentUser && currentUser.email}!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
