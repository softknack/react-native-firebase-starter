import React from "react";
import { StyleSheet, Platform, Image, Text, View } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";

// import the different screens
import Loading from "./components/Loading";
import SignUp from "./components/SignUp";
import Login from "./components/Login";
import Main from "./components/Main";
import PhoneSignup from "./components/PhoneSignup";

// create our app's navigation stack
const AppNavigator = createSwitchNavigator(
  {
    Loading,
    SignUp,
    PhoneSignup,
    Login,
    Main
  },
  {
    initialRouteName: "Loading"
  }
);

const App = createAppContainer(AppNavigator);

export default App;
